//
//  ViewController.swift
//  Animations
//
//  Created by Oroz on 1/9/22.
//

import UIKit
import SnapKit
import SPStorkController

class ViewController: UIViewController {
    
    lazy var btn: UIButton = {
        let view = UIButton()
        view.setTitle("BTN", for: .normal)
        view.backgroundColor = .red
        view.addTarget(self, action: #selector(pressToAction), for: .touchUpInside)
        return view
    }()
    
    lazy var btnSec: UIButton = {
        let view = UIButton()
        view.setTitle("BTN", for: .normal)
        view.backgroundColor = .red
        view.layer.cornerRadius = 10
        view.addTarget(self, action: #selector(pressToActionToOpen), for: .touchUpInside)
        return view
    }()
    
    lazy var img = UIImageView(image: UIImage(named: ""))
    
    var firstBtnLeftConstraint: Constraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url = URL(string: "https://www.undp.org/sites/g/files/zskgke326/files/migration/cn/UNDP-CH-Why-Humanity-Must-Save-Nature-To-Save-Itself.jpeg")

        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                self.img.image = UIImage(data: data!)
            }
        }
        
        
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
               blurView.translatesAutoresizingMaskIntoConstraints = false
               blurView.alpha = 0.9
               blurView.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
               view.insertSubview(img, at: 0)
               blurView.center.x = self.view.center.x
               blurView.center.y = self.view.center.y * 1.7
               
               blurView.layer.cornerRadius = 10.0
               blurView.clipsToBounds = true

        view.addSubview(img)
        img.snp.makeConstraints { make in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        view.addSubview(btn)
        btn.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.width.equalTo(150)
            make.centerY.equalToSuperview()
            firstBtnLeftConstraint = make.left.equalToSuperview().constraint
        }
        
        view.addSubview(blurView)
        blurView.snp.makeConstraints { make in
            make.height.equalTo(150)
            make.width.equalTo(150)
            make.centerY.centerX.equalToSuperview()
        }
        
        view.addSubview(btnSec)
        btnSec.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.width.equalTo(150)
            make.centerY.equalToSuperview()
            make.left.equalTo(btn.snp.right).offset(2)
        }
    }

    @objc func pressToAction(){
        
        UIView.animate(withDuration: 2.0, delay: 0.0, options: [.curveEaseInOut]) {
            self.firstBtnLeftConstraint?.update(inset: 100)
            self.btn.layer.cornerRadius = 10
            self.view.layoutIfNeeded()
        } completion: { isFinish in
            
        }

    }
    
    
    @objc func pressToActionToOpen(){
        
            let controller = SecondVC()
        let transitionDelegate = SPStorkTransitioningDelegate()
//        transitionDelegate.customHeight = 350
        transitionDelegate.tapAroundToDismissEnabled = true


        controller.transitioningDelegate = transitionDelegate
        controller.modalPresentationStyle = .custom
        controller.modalPresentationCapturesStatusBarAppearance = true
        self.present(controller, animated: true, completion: nil)

    }

}

