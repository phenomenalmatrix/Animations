//
//  SecondVC.swift
//  Animations
//
//  Created by Oroz on 1/9/22.
//

import Foundation
import UIKit

class SecondVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        let optionVC = ViewController()
        present(optionVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
}
